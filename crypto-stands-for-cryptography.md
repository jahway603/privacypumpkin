# Cryptocurrency privacy tips

Here we have some cryptocurrency tips for you. Please feel free to contribute from your experiences here so others may benefit too.

## Wallet advice 

* [Easy how to setup a Monero desktop wallet tutorial](https://open.lbry.com/@Luke:7/creating-a-monero-wallet-to-receive:7)
* If you use a phone wallet, then use one in which you can configure the full node server it is connecting to.
  * For example, Edge wallet let's you do this with Bitcoin, but the feature is currently not available for Monero on Edge, so you want [Monerujo](https://odysee.com/@monerocommunityworkgroup:8/andr%C3%A9s-why-we-monerujo:5) then.
* Time has shown us that the best privacy and security always comes with using native wallets written by the coin's devs vs 3rd party wallets

## Advanced Cryptocurrency topics

For those who know

* If you are able to, run your own full nodes for the coins you use. 
  * If using Monero, there are two great projects for you to "easily" run a full node on a separate device that you can connect to from your phone light wallet or any desktop wallet.
    1. [PiNodeXMR](https://moneroecosystem.org/pinode-xmr/), which will run on a Raspberry Pi 4 or a Pine64 SBC.
    1. If you are running a regular Intel or AMD server (known as x86 architecture), then you can easily setup a full node and run it using Docker with [docker-monero-node](https://codeberg.org/jahway603/docker-monero-node).
  * Also this [Video how to setup your own Monero node on a Raspberry Pi](https://yewtu.be/watch?v=riK8t_4llXw)

## Contribute
Feel free to fork and contribute back. Let's build this together!
