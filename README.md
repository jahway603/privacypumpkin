# privacypumpkin

![two pumpkins hanging out](images/funny-face-on-fall-pumpkins-picture-id177572791.jpeg)

This document contains information regarding privacy. For anyone local to me, you will get the reference to the pumpkin ;)

## Real life privacy tips

* If you have one in your area, use a private mailbox for better protection. As an added bonus, the packages will go there when you're not home, so a less chance of theft too.
* In New Hampshire, there exists a "Driver's License Privacy form", called a DSMV 499, which they have now completely removed from their website and can only be seen as a [blank 2nd page in this pdf here](https://www.dmv.nh.gov/sites/g/files/ehbemt416/files/inline-documents/dsmv490.pdf). They want you to write them privately specifically asking for one. I thought that to be BS, so did a 91A (NH FOIA) request and [all revisions of DSMV499 are here](pdf/DSMV499). If you live in another State, then your state might have a similar form or not.

## "Privacy" or "Security" Companies to be aware of

* Don't use Brave browser, as ["Brave Browser caught adding its own referral codes to some cryptocurrency trading sites"](https://www.androidpolice.com/2020/06/07/brave-browser-caught-adding-its-own-referral-codes-to-some-cryptcurrency-trading-sites/)
* Don't use Signal messenger for numerous reasons but they recently implemented a Surveillance Cryptocurrency into their app behind everyone's back. [Learn more here](https://www.bitchute.com/video/tJoO2uWrX1M/)
* Don't use Keybase, as [they cannot be trusted after being bought by Zoom](https://www.cnbc.com/2020/05/07/zoom-buys-keybase-in-first-deal-as-part-of-plan-to-fix-security.html)
* Don't use Microsoft or Apple operating systems or products as they do not value privacy and exploit user info
* Don't use Discord, as [it is spyware](https://spyware.neocities.org/articles/discord) and should be avoided at all costs

## Web browser advice

I created a [separate file](web.md) for these tips....

## Cryptocurrency privacy tips

I created a [separate file](crypto-stands-for-cryptography.md) for these tips....

## Contribute
Feel free to fork and contribute back. Let's build this together!
