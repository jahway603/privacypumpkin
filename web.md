# Web Privacy/Security Advice

Right now I'm laughing that the filename of this is "web.md", but let's get on to the web stuff...

## Web browser recommendations

* [Chromium](https://chromium.woolyss.com/download/) is a great alternative for the Chrome fans. It is available on my operating systems.
* If you enjoy FireFox, then check out [LibreWolf](https://librewolf-community.gitlab.io/install/) and, yes it works on Win/Mac if you're still stuck there, but it is a bit extreme so be ready for that.
* Don't use Brave browser, as ["Brave Browser caught adding its own referral codes to some cryptocurrency trading sites"](https://www.androidpolice.com/2020/06/07/brave-browser-caught-adding-its-own-referral-codes-to-some-cryptcurrency-trading-sites/), unless you like shady web browser behaviour.
* Only if you enjoy VI/VIM keybindings, then check out [QuteBrowser](https://qutebrowser.org/doc/install.html). If you don't know what that means, then I wouldn't recommend this browser to you.

## Web browser plugin recommendations

The above web browsers are great, but you still need these for optimal privacy:

| Plugin Name | Description | Chrome-ium Download link | Firefox Download link |
| :---        | ----------- | ------------------------ | --------------------- |
| [EFF's HTTPS Everywhere](https://www.eff.org/https-everywhere) | Tells the web browser to always try viewing a web site securely with SSL | [Click here](https://chrome.google.com/webstore/detail/gcbommkclmclpchllfjekcdonpmejbdp) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/) |
| [uBlock Origin](https://ublockorigin.com/) | Free, open-source ad content blocker | [Click here](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) |
| [FSF's JShelter (or JavaScript Restrictor)](https://jshelter.org/) & [more info](https://www.ghacks.net/2021/10/04/javascript-restrictor-improve-privacy-by-limiting-web-browser-apis/) | This plugin [was created to combat all the online javascript garbage](https://www.fsf.org/news/fsf-announces-jshelter-browser-add-on-to-combat-threats-from-nonfree-javascript) | [Click here](https://chrome.google.com/webstore/detail/javascript-restrictor/ammoloihpcbognfddfjcljgembpibcmb) | [Click here](https://addons.mozilla.org/firefox/addon/javascript-restrictor/) |
| [Clear URLs](https://gitlab.com/KevinRoebert/ClearUrls) | This plugin will [remove all the tracking cookies](https://www.ghacks.net/2019/07/30/clearurls-extension-firefox-chrome/) (like ?fbcli=) from your web links automatically. This is great for when you forget to remove the tracking cookie before sharing the weblink publicly. | [Click here](https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/clearurls/) |
| [Privacy Badger](https://privacybadger.org/) | This plugin automatically learns to block invisible trackers. | [Click here](https://chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp?hl=en-US) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/) |

The plugins below act all fudgy unless you properly configure them, so please read how to do that or find a video of someone doing that or you will encounter issues:

| Plugin Name | Description | Chrome-ium Download link | Firefox Download link |
| :---        | ----------- | ------------------------ | --------------------- |
| [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect) | If configured properly, this plugin will automatically use privacy-oriented frontends to big tech services such as using [Invidious](https://redirect.invidious.io/) for YouTube | [Click here](https://chrome.google.com/webstore/detail/privacy-redirect/pmcmeagblkinmogikoikkdjiligflglb) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) |
| [NoScript](https://noscript.net/) ([source code](https://github.com/hackademix/noscript)) | NoScript also provides the most powerful anti-XSS and anti-Clickjacking protection ever available in a browser. | [Click here](https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm?hl=en-US) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/noscript/) |
| [LocalCDN](https://codeberg.org/nobody/LocalCDN/) | This plugin emulates Content Delivery Networks (CDNs) to improve your online privacy. It intercepts traffic, finds supported resources locally, and injects them into the environment | [Click here](https://chrome.google.com/webstore/detail/localcdn/njdfdhgcmkocbgbhcioffdbicglldapd/) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes/) |

The plugins below help you support networks you believe in supporting:

| Plugin Name | Description | Chrome-ium Download link | Firefox Download link |
| :---        | ----------- | ------------------------ | --------------------- |
| [Snowflake](https://snowflake.torproject.org/) | This allows users in censored countries to use your network connection to access the Tor network. | [Click here](https://chrome.google.com/webstore/detail/snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie) | [Click here](https://addons.mozilla.org/en-US/firefox/addon/torproject-snowflake/) |

## Contribute
Feel free to fork and contribute back. Let's build this together!
